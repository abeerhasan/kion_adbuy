//
//  InputTextField.swift
//  ADBUY-KION
//
//  Created by Mohammed Mohsin Sayed on 8/31/20.
//  Copyright © 2020 alaa salama. All rights reserved.
//



import UIKit
import TKFormTextField

class CustomizedTextField: TKFormTextField {// UITextField{
    //--- Resize font----------------------------
       @IBInspectable
       var fontSize : CGFloat = 19 {
           didSet{
               self.font = UIFont(name: self.font!.fontName, size: autoFontSize(interedFontSize: fontSize))
           }
       }
    
    //--- Variables -------------------------------------------------
    var showErrorButton = UIButton()
    var validationType = ValidationType.name
    var lastError = ErrorContent.fieldRequired
    //var leftViewButton = UIButton()
    
    //----------------------------------------------------------------
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //--- Decoration ---------------------
        decorationSetup()
        
        //--- Validation, Targets ------------------------
        getValidationType()
        targetSetup()
    }
    
    //--- helper functions -------------------------------------------
    func autoFontSize(interedFontSize: CGFloat) -> CGFloat {
        let sceenWidth = UIScreen.main.bounds.width
        if sceenWidth >= 568 {
            return interedFontSize * 2
        }
        return interedFontSize
    }
    
    func decorationSetup(){
        let placeHolder = NSAttributedString(string: self.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : Light_Text])
        self.attributedPlaceholder = placeHolder
        
        self.showErrorButton = .systemButton(with: UIImage(systemName: "exclamationmark.octagon")!, target: self, action: #selector(showError))
        self.showErrorButton.alpha = 0.6
        
       // self.leftViewButton = .systemButton(with: UIImage(systemName: "eye")!, target: self, action: #selector(show_hideText))
        //self.leftViewButton.alpha = 0.6
        
        self.lineView.removeFromSuperview()
        
        self.titleColor = Light_Text
        self.selectedTitleColor = UIColor.white
        
        self.errorLabel.textColor = Light_Text
    }
    
    /*@objc func show_hideText(){
        if leftViewButton.image(for: .normal) == UIImage(systemName: "eye") {
            self.leftViewButton.removeFromSuperview()
            self.leftViewButton.setImage(UIImage(systemName: "eye.slash"), for: .normal)
            self.isSecureTextEntry = false
        }else {
            self.leftViewButton.removeFromSuperview()
            self.leftViewButton.setImage(UIImage(systemName: "eye"), for: .normal)
            self.isSecureTextEntry = true
        }
    }*/
    
}
