//
//  InputTextField_Ext.swift
//  ADBUY-KION
//
//  Created by Mohammed Mohsin Sayed on 9/14/20.
//  Copyright © 2020 alaa salama. All rights reserved.
//

import UIKit

extension CustomizedTextField {
    
     
    func setError(error: String){
        self.lastError = error
    }
    func getError() -> String {
        return self.lastError
    }

    func targetSetup(){
        self.addTarget(self, action: #selector(checkFieldVlidation), for: .editingChanged)
        self.addTarget(self, action: #selector(hideErrorLabel), for: .editingDidEnd)
    }
    
    @objc func checkFieldVlidation() {
        //--- text begining don't accept space---------------
        if self.text == " " {
            self.text = ""
        }

        Validation.fieldIsFullAndValid(fieldContent: self.text!, type: self.validationType) { (result) in
          switch result {
            case .failure(errReason: let error):
                showErrorWorning()
                self.setError(error: error)
                self.error = error
            case .success :
                hideErrorWorning()
            }
        }
    }
    
    //--- Helper Functions ----------------------------------------------
    func getValidationType() {
        var validationType_: ValidationType
            
        if self.placeholder == "Email" {
            validationType_ = .email
        }else if self.placeholder == "Password" {
            validationType_ = .password
        }else if self.placeholder == "Phone number" {
            validationType_ = .phone
        }else if self.placeholder == "Name" {
            validationType_ = .name
        } else {
            validationType_ = .confirmPassword
        }
        
        self.validationType = validationType_
    }
    //------------------------------------------------------------------
    func showErrorWorning(){
        self.rightView = self.showErrorButton
        self.rightViewMode = .always
    }
    
    func hideErrorWorning(){
        self.rightViewMode = .never
        hideErrorLabel()
        self.setError(error: "")
    }
    
    @objc func showError(){
        var error = ""
        if self.getError() == ErrorContent.fieldRequired{
            error = "\(self.placeholder ?? "This field") is required please fill it"
        }else {
            error = self.getError()
        }
        parentContainerViewController?.showErrorXIB(error: error)
    }
    
    @objc func hideErrorLabel(){
        self.error = ""
    }
    func clearText(){
        self.text = ""
    }
}
