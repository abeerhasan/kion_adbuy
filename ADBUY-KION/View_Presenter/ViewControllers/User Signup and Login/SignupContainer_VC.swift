//
//  ViewController.swift
//  ADBUY-KION
//
//  Created by alaa salama on 8/29/20.
//  Copyright © 2020 alaa salama. All rights reserved.
//

import UIKit

var currentForm = FormName.Signup

class SignupContainer_VC: UserAuth_VC{

    override func viewDidLoad() {
        super.viewDidLoad()
        currentForm = FormName.Signup
    }
  
    //---Actions----------------------------------
    
    @IBAction func skipButtonClicked(_ sender: Any) {
         self.goHome()
    }
}

