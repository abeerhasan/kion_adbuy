//
//  Validation.swift
//  ADBUY-KION
//
//  Created by Mohamed Tarek on 9/9/20.
//  Copyright © 2020 alaa salama. All rights reserved.
//

import Foundation

enum FormName: String {
    case Login = "Login"
    case Signup = "Signup"
    case ForgotPassword = "Forget Password"
}

enum ValidationType: String{
    case email = "email ex.someName@something.com"
    case phone = "phone number should be 10 numbers ex.(0)XXXYYYZZZ"
    case password = "Password Should contain at least 8 chars, 1 no, upper and lower case letter"
    case name = "name Should contain letters only"
    case confirmPassword
}

enum ValidationResult<errReason,type> {
    case success
    case failure(errReason: String)
}


struct ErrorContent {
    static var fieldRequired = "Required *"
    static var notValid = "Not Valid"
}
struct Validation {
    
    
   //---- Check one field only ---------------------------------------------------------
    static func fieldIsFullAndValid(fieldContent: String, type: ValidationType, completion: (ValidationResult<String, ValidationType>) -> ()){
       if fieldContent.isEmpty {
           completion(.failure(errReason: ErrorContent.fieldRequired))
       }else if fieldContent.isValid(type) || ( currentForm == .Login && type == .password) || type == .confirmPassword {
           completion(.success)
       }else if !fieldContent.isValid(type) {
        completion(.failure(errReason: "\(ErrorContent.notValid), \(type.rawValue)"))
       }
    }
}


