//
//  Error_VC_xib.swift
//  ADBUY-KION
//
//  Created by Mohammed Mohsin Sayed on 9/11/20.
//  Copyright © 2020 alaa salama. All rights reserved.
//

import UIKit

class Error_VC_xib: UIViewController {


    @IBOutlet weak var okButton: CustomizedButton!
    @IBOutlet weak var errorMessageLabel: CustomLabelFontSize!
   
    //----------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        okButton.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.4) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform.identity
        }
    }
    
    func setUpView(){
        NotificationCenter.default.addObserver(self, selector: #selector(didGetNotification(_:)), name: NSNotification.Name("Error"), object: nil)
        
        //close the xib after screen tap
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
                 view.addGestureRecognizer(tap)
    }
    //-----------------------------------------------
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        
    }
    //--- Actions -----------------------------------
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        handleTap()
    }
    
    @IBAction func okButtonClicked(_ sender: Any) {
        
    }
    
    //-----------------------------------------------
    @objc func didGetNotification(_ notification: Notification){
        let errorMessage = notification.object as! String
        errorMessageLabel.text = errorMessage
    }
         
    @objc func handleTap(){
        UIView.animate(withDuration: 0.3, animations: {
            self.view.transform = CGAffineTransform.init(scaleX: 1.3, y: 1)
            self.view.alpha = 0
        }) { (success) in
             self.dismiss(animated: true, completion: nil)
        }
    }
}
